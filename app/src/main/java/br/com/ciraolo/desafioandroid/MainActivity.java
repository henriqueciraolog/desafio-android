package br.com.ciraolo.desafioandroid;

import android.content.res.Configuration;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import br.com.ciraolo.desafioandroid.adapter.ShotRecyclerViewAdapter;
import br.com.ciraolo.desafioandroid.beans.Shot;
import br.com.ciraolo.desafioandroid.listerner.EndlessRecyclerViewScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<List<Shot>>, SwipeRefreshLayout.OnRefreshListener {

    public static final String MAIS_COMENTADAS = "comments";
    public static final String MAIS_VISUALIZADAS = "views";
    public static final String MAIS_RECENTES = "recent";

    API api = new Retrofit.Builder()
            .baseUrl("https://api.dribbble.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(API.class);

    @BindView(R.id.rcvShots)
    RecyclerView rcvShots;

    @BindView(R.id.srlAtualizar)
    SwipeRefreshLayout srlAtualizar;

    private ShotRecyclerViewAdapter rcvAdapter;

    private String ordenacao = MAIS_VISUALIZADAS;

    boolean fimDaPagina = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupLayout();

        srlAtualizar.post(new Runnable() {
            @Override
            public void run() {
                srlAtualizar.setRefreshing(true);
            }
        });

        getSupportActionBar().setTitle(R.string.titulo_mais_visualizadas);
        getShots(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ordenar_comentadas: {
                getSupportActionBar().setTitle(R.string.titulo_mais_comentadas);
                srlAtualizar.setRefreshing(true);
                rcvAdapter.updateList(new ArrayList<Shot>());
                ordenacao = MAIS_COMENTADAS;
                getShots(1);
                return true;
            }
            case R.id.ordenar_visualizacoes: {
                getSupportActionBar().setTitle(R.string.titulo_mais_visualizadas);
                srlAtualizar.setRefreshing(true);
                rcvAdapter.updateList(new ArrayList<Shot>());
                ordenacao = MAIS_VISUALIZADAS;
                getShots(1);
                return true;
            }
            case R.id.ordernar_recentes: {
                getSupportActionBar().setTitle(R.string.titulo_mais_recentes);
                srlAtualizar.setRefreshing(true);
                rcvAdapter.updateList(new ArrayList<Shot>());
                ordenacao = MAIS_RECENTES;
                getShots(1);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getShots(int page) {
        fimDaPagina = false;
        Call<List<Shot>> call = api.fetchShots(page, 50, ordenacao);
        call.enqueue(this);
    }

    private void setupLayout() {
        srlAtualizar.setOnRefreshListener(this);
        rcvShots.setHasFixedSize(true);

        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        int qtdeColunas = display.getRotation() == Surface.ROTATION_0 || display.getRotation() == Surface.ROTATION_180 ? 4 : 8;

        GridLayoutManager rcvLayoutManager = new GridLayoutManager(this, qtdeColunas);
        rcvShots.setLayoutManager(rcvLayoutManager);

        //ADAPTER
        rcvAdapter = new ShotRecyclerViewAdapter(this, new ArrayList<Shot>());
        rcvShots.setAdapter(rcvAdapter);

        rcvShots.addOnScrollListener(new EndlessRecyclerViewScrollListener(rcvLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (!fimDaPagina) getShots(page + 1);
            }
        });
    }

    @Override
    public void onResponse(Call<List<Shot>> call, Response<List<Shot>> response) {
        srlAtualizar.setRefreshing(false);
        if (response.code() == 200) {
            List<Shot> shots = response.body();
            if (shots.size() > 0) rcvAdapter.addList(shots);
            else fimDaPagina = true;
        } else {
            fimDaPagina = true;
        }
    }

    @Override
    public void onFailure(Call<List<Shot>> call, Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onRefresh() {
        rcvAdapter.updateList(new ArrayList<Shot>());
        getShots(1);
    }
}
