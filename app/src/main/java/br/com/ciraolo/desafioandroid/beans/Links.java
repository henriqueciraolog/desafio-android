package br.com.ciraolo.desafioandroid.beans;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Links implements Serializable {

    private String web;
    private String twitter;

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}