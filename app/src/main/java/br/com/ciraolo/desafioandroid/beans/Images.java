package br.com.ciraolo.desafioandroid.beans;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Images implements Serializable {

    private String hidpi;
    private String normal;
    private String teaser;

    public String getHidpi() {
        return hidpi;
    }

    public void setHidpi(String hidpi) {
        this.hidpi = hidpi;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}