package br.com.ciraolo.desafioandroid;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import br.com.ciraolo.desafioandroid.beans.Shot;
import br.com.ciraolo.desafioandroid.util.FileUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION_WRITE_READ_STORAGE = 100;

    public static final String SHOT = "shot";
    @BindView(R.id.imgShotNormal)
    ImageView imgShotNormal;

    @BindView(R.id.txvDesc)
    TextView txvDesc;

    @BindView(R.id.txvAutor)
    TextView txvAutor;

    private Shot shot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        shot = (Shot) getIntent().getSerializableExtra(SHOT);

        setupLayout();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupLayout() {

        //Configura o Back Buton
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Configura o título da activity
        if (shot.getTitle() != null) getSupportActionBar().setTitle(shot.getTitle());

        //Carrega a imagem
        Picasso.with(this).load(shot.getImages().getHidpi()).error(R.drawable.ic_error_outline_black_100dp).into(imgShotNormal);

        //Configura a descrição da imagem, tirando os HTMLs.
        txvDesc.setText(Html.fromHtml(shot.getDescription() != null ? shot.getDescription() : "").toString());

        //Configura o autor da imagem.
        txvAutor.setText(shot.getUser().getName());

    }

    @OnClick(R.id.btnCompartilhar)
    public void compartilhar() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(imgShotNormal, R.string.precisa_permissao, Snackbar.LENGTH_LONG).setAction(R.string.autorizar, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(DetailsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_READ_STORAGE);
                }
            }).show();
        } else {
            compartilharFoto();
        }
    }

    private void compartilharFoto() {
        Uri bmpUri = FileUtil.getLocalBitmapUri(imgShotNormal);
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            // Launch sharing dialog for image
            startActivity(Intent.createChooser(shareIntent, getString(R.string.compartilhar_imagem)));
        } else {
            Snackbar.make(imgShotNormal, R.string.erro_compartilhar, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_PERMISSION_WRITE_READ_STORAGE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    compartilhar();
                } else {
                    Snackbar.make(imgShotNormal, R.string.sem_permissao, Snackbar.LENGTH_SHORT).show();
                }
            }

        }
    }
}
