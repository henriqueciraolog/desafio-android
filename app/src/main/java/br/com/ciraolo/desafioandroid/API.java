package br.com.ciraolo.desafioandroid;

import java.util.List;

import br.com.ciraolo.desafioandroid.beans.Shot;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by henriqueciraolo on 28/05/17.
 */

public interface API {

    @GET("shots")
    @Headers({"Authorization: Bearer 36cb717ff184c871a8e5b7824a174aa792bdedbb7b28cd3fd9cf538bb7adef48"})
    Call<List<Shot>> fetchShots(@Query("page") int page, @Query("per_page") int perPage, @Query("sort") String sort);
}
