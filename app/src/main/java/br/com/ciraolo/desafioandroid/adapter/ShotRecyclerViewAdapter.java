package br.com.ciraolo.desafioandroid.adapter;

//================================================================================
// IMPORTS
//================================================================================

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.ciraolo.desafioandroid.DetailsActivity;
import br.com.ciraolo.desafioandroid.MainActivity;
import br.com.ciraolo.desafioandroid.R;
import br.com.ciraolo.desafioandroid.beans.Shot;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShotRecyclerViewAdapter extends RecyclerView.Adapter<ShotRecyclerViewAdapter.ViewHolder> {

    //================================================================================
    // PROPERTIES
    //================================================================================
    private List<Shot> mDataset;
    private final Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        //PROPERTIES
        @BindView(R.id.viewShot)
        View viewShot;
        @BindView(R.id.imgShot)
        ImageView imgShot;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    //================================================================================
    // CONSTRUCTOR
    //================================================================================
    // Provide a suitable constructor (depends on the kind of dataset)
    public ShotRecyclerViewAdapter(Context context, List<Shot> myDataset) {
        mContext = context;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shot_adapter_view, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Shot shot = mDataset.get(position);

        holder.viewShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra(DetailsActivity.SHOT, shot);

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        (MainActivity) mContext,
                        new Pair<View, String>(holder.imgShot,
                                mContext.getString(R.string.transition_name_shot))
                );
                ActivityCompat.startActivity(mContext, intent, options.toBundle());
            }
        });
        Picasso.with(mContext).load(shot.getImages().getNormal()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.imgShot, new Callback() {
            @Override
            public void onSuccess() {
                //Sucesso!!
            }

            @Override
            public void onError() {
                Picasso.with(mContext).load(shot.getImages().getNormal()).error(R.drawable.ic_error_outline_black_100dp).into(holder.imgShot);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateList(List<Shot> newlist) {
        mDataset = new ArrayList<>();
        mDataset.addAll(newlist);
        notifyDataSetChanged();
    }

    public void addList(List<Shot> addlist) {
        int pos = mDataset.size();
        mDataset.addAll(addlist);
        notifyItemInserted(pos);
    }

    public interface OnItemClickListerner {
        void onItemClick(Shot linha);
    }
}



